-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 23, 2019 at 02:16 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smartstick`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `email` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`email`, `password`) VALUES
('afiqahrosman@gmail.com', '12345');

-- --------------------------------------------------------

--
-- Table structure for table `guardian`
--

CREATE TABLE `guardian` (
  `name` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `no_tel` varchar(50) NOT NULL,
  `ic` varchar(14) NOT NULL,
  `address` varchar(50) NOT NULL,
  `poscode` varchar(10) NOT NULL,
  `state` varchar(50) NOT NULL,
  `area` varchar(50) NOT NULL,
  `relation` varchar(50) NOT NULL,
  `gender` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guardian`
--

INSERT INTO `guardian` (`name`, `password`, `email`, `no_tel`, `ic`, `address`, `poscode`, `state`, `area`, `relation`, `gender`) VALUES
('Nurfiqah Rosman', 'Afiqah95', 'afiq@gmail.com', '0194766246', '870420025650', '5055 Lorong selasih 1/6,, Taman Selasih 5,', '09000', 'Johor', 'Pasir Mas', 'kakak', 'Female'),
('a', 'Afiqah95', 'afiqa@gmail.com', '1', '987654342134', '123', '12345', 'Kelantan', 'Pasir Mas', 'Q', 'Female'),
('Nurafiqah Rosman', 'Afiqah95', 'afiqah8@gmail.com', '0194766246', '972783459890', '5055 Lorong selasih 1/6,, Taman Selasih 5,', '09000', 'Kedah', 'Kubang Pasu', 'adik', 'Male'),
('Nurafiqah rosman', 'Afiqah95', 'afiqahrosman95@gmail.com', '0194766246', '950420025650', '5055 Lorong selasih 1/6,, Taman Selasih 5,', '09111', 'Pahang', 'Klang', 'Kakak', ''),
('kamek', 'Afiqah95', 'afiqaj@gmail.com', '1', '879090989090', 'taman melawati', '98000', 'Kelantan', 'Kota Bharu', 'adik', 'Female');

-- --------------------------------------------------------

--
-- Table structure for table `navigation`
--

CREATE TABLE `navigation` (
  `ic_patient` varchar(14) DEFAULT NULL,
  `latitude` varchar(50) NOT NULL,
  `longitude` varchar(50) NOT NULL,
  `nav_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE `patient` (
  `ic_patient` varchar(14) NOT NULL,
  `email` varchar(50) NOT NULL,
  `no_oku` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `no_tel` varchar(50) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `poscode` varchar(10) NOT NULL,
  `state` varchar(50) NOT NULL,
  `area` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`ic_patient`, `email`, `no_oku`, `name`, `address`, `no_tel`, `gender`, `poscode`, `state`, `area`) VALUES
('970420025650', 'afiqahrosman95@gmail.com', 'tba123456', 'Aiysa', '5055 Lorong selasih 1/6,, Taman Selasih 5,', '0194766246', 'Male', '09000', 'Melaka', 'Alor Gajah');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `guardian`
--
ALTER TABLE `guardian`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `navigation`
--
ALTER TABLE `navigation`
  ADD PRIMARY KEY (`nav_id`),
  ADD KEY `navigation_ibfk_1` (`ic_patient`);

--
-- Indexes for table `patient`
--
ALTER TABLE `patient`
  ADD PRIMARY KEY (`ic_patient`),
  ADD KEY `patient_ibfk_1` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `navigation`
--
ALTER TABLE `navigation`
  MODIFY `nav_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `navigation`
--
ALTER TABLE `navigation`
  ADD CONSTRAINT `navigation_ibfk_1` FOREIGN KEY (`ic_patient`) REFERENCES `patient` (`ic_patient`);

--
-- Constraints for table `patient`
--
ALTER TABLE `patient`
  ADD CONSTRAINT `patient_ibfk_1` FOREIGN KEY (`email`) REFERENCES `guardian` (`email`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
